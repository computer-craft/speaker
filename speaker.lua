speaker = peripheral.find("speaker")

-- https://minecraft.gamepedia.com/Sounds.json
if speaker then
    local midi = dofile("/lib/apis/midi.lua")
    -- speaker.playSound("minecraft:entity.ghast.shoot")

    midi.playFile("tetris.mid", speaker.playSound)
else
    print("No speaker has been found")
end