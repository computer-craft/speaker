local projectId = 12609315
local projectAPI = "https://gitlab.com/api/v4/projects/" .. projectId .. "/repository"

local arguments = { ... }

local jsonLib = "/lib/apis/json.lua"

if fs.exists(jsonLib) == false then
    shell.run("wget", "https://raw.githubusercontent.com/rxi/json.lua/master/json.lua", jsonLib)
end

local JSON = dofile(jsonLib)

if #arguments == 1 then
    local action = arguments[1]

    if action == "pull" then
        print("git: Pulling repository " .. projectAPI)
        local api = http.get(projectAPI .. "/tree?recursive=true")
        local res = api.readAll()
        api.close() -- close as soon as possible

        local fileTree = JSON.decode(res)
        print(fileTree)

        for key, value in ipairs(fileTree) do
            -- GET /projects/:id/repository/blobs/:sha/raw
            if (value.type == "blob") then 
                -- TODO: Read SHA hash for file change
                shell.run("rm", value.path)
                shell.run("wget", projectAPI .. "/blobs/" .. value.id .. "/raw", value.path)
            else
                print("Skipped SHA " .. value.id)
            end
        end

        print("git: Pull complete!")
    else
        print("git: Unimplemented")
    end
end