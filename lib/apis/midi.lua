------------------------------------
-- Falco's MIDI parser
--
-- based on: https://github.com/NHQ/midi-file-parser/blob/master/index.js
------------------------------------
local this = {}

this = { 
    version = "0.0.1-prerelease",

    types = {
        ["SEQUENCE_NUM"] = 0,
        ["TEXT"] = 1,
        ["COPYRIGHT"] = 2,
        ["SEQUENCE_NAME"] = 3,
        ["INSTRUMENT_NAME"] = 4,
        ["LYRIC"] = 5,
        ["MARKER"] = 6,
        ["CUE_POINT"] = 7,
        ["PROGRAM_NAME"] = 8,
        ["DEVICE_NAME"] = 9,
        ["MIDI_CHANNEL_PREFIX"] = 0x20,
        ["MIDI_PORT"] = 0x21,
        ["END_OF_TRACK"] = 0x2f,
        ["TEMPO"] = 0x51,
        ["SMPTE_OFFSET"] = 0x54,
        ["TIME_SIGNATURE"] = 0x58,
        ["KEY_SIGNATURE"] = 0x59,
        ["SEQUENCER_EVENT"] = 0x7f,
    },
    
    playFile = function (src, callback)
        -- callback("minecraft:entity.ghast.shoot")
        this.read(src)
        -- this.speaker = callback
    end,
    
    read = function (src)
        local file = fs.open(src, "rb")

        if file then
            this.file = file

            local header = this.readChunk()

            -- Header Chunk length must be 6 bytes in order to read 3x int16
            local chunk = {
                -- format type
                ["formatType"] = this.uint16({
                    header.data[1],
                    header.data[2]
                }),
                -- track count
                ["trackCount"] = this.uint16({
                    header.data[3],
                    header.data[4]
                }),
                -- time division
                ["ticksPerBeat"] = this.uint16({
                    header.data[5],
                    header.data[6]
                })
            }

            -- print(header.id) -- must be: MThd
            -- print(header.length) -- must be: 6
            print("Midi Header, " .. header.length .. " bytes")
            print("- Format type    : " .. chunk.formatType)
            print("- Track count    : " .. chunk.trackCount)
            print("- Ticks per beat : " .. chunk.ticksPerBeat)

            local tracks = {}

            for int = 1, chunk.trackCount do
                local chunk = this.readChunk()
                -- todo parse MTrk data
                
                if chunk.id == "MTrk" and chunk.length == 122 then -- temporary condition for debugging
                    print("Midi Track, " .. chunk.length .. " bytes")
                    table.insert(tracks, chunk)

                    this.parseEvents(chunk.data)
                end
            end

            -- print(tracks)

            this.close()
        else
            printError("Unable to read: " .. src)
        end
    end,

    parseEvents = function (data) -- MTrk data
        local index = 1
        while index <= #data do
            local delta = this.uint8(data[index])
            local status = this.uint8(data[index + 1])

            if bit32.band(status, 0xf0) == 0xf0 then -- Non-MIDI event / Meta event
                local type = this.uint8(data[index + 2])
                local length = this.uint8(data[index + 3])

                print("Event #" .. status .. " (" .. delta .. ", " .. type .. ", " .. length  .. ") index @ " .. index)

                if status == 255 then
                    if type == this.types.SEQUENCE_NUM then
                    elseif type == this.types.TEXT then
                    elseif type == this.types.COPYRIGHT then
                    elseif type == this.types.SEQUENCE_NAME then
                    elseif type == this.types.INSTRUMENT_NAME then
                    elseif type == this.types.LYRIC then
                    elseif type == this.types.MARKER then
                    elseif type == this.types.CUE_POINT then
                    elseif type == this.types.PROGRAM_NAME then
                    elseif type == this.types.DEVICE_NAME then
                        print("Device Name found")
                    elseif type == this.types.MIDI_CHANNEL_PREFIX then
                    elseif type == this.types.MIDI_PORT then
                    elseif type == this.types.END_OF_TRACK then
                        print("End of MTrk! Index: " .. index)
                    elseif type == this.types.TEMPO then
                    elseif type == this.types.SMPTE_OFFSET then
                    elseif type == this.types.TIME_SIGNATURE then
                    elseif type == this.types.KEY_SIGNATURE then
                    elseif type == this.types.SEQUENCER_EVENT then
                    end
                elseif status == 240 then -- sysEx
                elseif status == 247 then -- dividedSysEx
                end
                
                -- print("Meta Event:")
                -- print(" - Delta  : " .. delta)
                -- print(" - Status : " .. status)
                -- print(" - Type   : " .. type)
                -- print(" - Length : " .. length)

                index = index + length + 4
            else
                print("Unimplemented channel event, index: " .. index)
                break
            end
        end
    end,

    readChunk = function ()
        local id = this.readString(4)
        local length = this.uint32()

        if id ~= "MThd" and id ~= "MTrk" then
            error("Unknown chunk, id: " .. id)
        end

        return {
            ["id"] = id,
            ["length"] = length,
            ["data"] = this.readBytes(length)
        }
    end,

    -- Read single byte as decimal
    readByte = function ()
        return this.file.read()
    end,

    -- Read sequential bytes
    readBytes = function (length)
        if length <= 0 then 
            return false
        else
            local bytes = {}
            for int = 1, length do
                local byte = this.readByte()
                
                if byte == nil then 
                    break 
                end

                table.insert(bytes, byte) -- decimal
            end

            return bytes
        end
    end,

    -- Read string as ascii
    readString = function (length)
        if length <= 0 then 
            return false
        else
            local text = ""
            for int = 1, length do
                local byte = this.readByte()
                
                if byte == nil then 
                    break 
                end
                
                text = text .. string.char(byte)
            end

            return text
        end
    end,

    uint = function (length, bytes) -- unsigned int
        if bytes == nil then
            bytes = this.readBytes(length)
        end

        local hex = ""

        for key, value in ipairs(bytes) do
            hex = string.format("%s%02x", hex, value)
        end

        return tonumber(hex, 16)
    end,

    uint8 = function (byte) -- 8 bit (1 byte)
        return this.uint(1, { byte })
    end,

    uint16 = function (bytes) -- 16 bit (2 bytes) | unsigned short
        return this.uint(2, bytes)
    end,

    uint32 = function (bytes) -- 32 bit (4 bytes)
        return this.uint(4, bytes)
    end,

    -- Close file stream
    close = function ()
        this.file.close()
    end
}

return this